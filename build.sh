#!/bin/sh -ex

# aws-cli really wants a specific version, this isn't it.
#PYINSTALLER_VERSION=5.13.2

# for reference
uname -a
ls -l /lib/ld-*
set | sort

# install build dependency packages
apk add \
  build-base \
  cmake \
  git \
  libffi-dev \
  libunwind-dev \
  openssl-dev \
  perl \
  zlib-dev
# causing problems?
#  cmocka-dev \

# get aws-cli v2 source
git clone --recursive --depth 1 --single-branch --branch v2 \
  https://github.com/aws/aws-cli.git
cd aws-cli

# aws-cli really wants a specific version, this isn't it.
#git clone --depth 1 --single-branch \
#  --branch v$PYINSTALLER_VERSION \
#  https://github.com/pyinstaller/pyinstaller.git \
#  /tmp/pyinstaller
# 2024-03-01 - this picks an old pyinstaller that doesn't work with 3.12
git clone --depth 1 --single-branch \
  --branch v$(grep PyInstaller requirements-build.txt | cut -d= -f3) \
  https://github.com/pyinstaller/pyinstaller.git \
  /tmp/pyinstaller
cd /tmp/pyinstaller/bootloader
# configure pyinstaller
CFLAGS="-Wno-stringop-overflow -Wno-stringop-truncation" \
  python ./waf configure --no-lsb all
# pip install this pyinstaller
pip install ..

# go back to aws-cli
cd -
# build aws cli executable
scripts/installers/make-exe
# unzip and install it
unzip dist/awscli-exe.zip
./aws/install

# done
