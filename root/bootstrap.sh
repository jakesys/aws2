#!/bin/sh

set -ex

apk --no-cache upgrade --available
apk --no-cache add groff

AWSCLI_DIR=/usr/local/aws-cli/v2/current
ln -s $AWSCLI_DIR/bin/aws /usr/local/bin

# set .version
aws --version 2>&1 | cut -d/ -f2 | cut -d' ' -f1 > /.version

# trim shell completion stuff
find $AWSCLI_DIR/dist/awscli/data -mindepth 1 -maxdepth 1 -type d -print -exec rm -rf {} \;
rm -rf \
  $AWSCLI_DIR/bin/aws_completer \
  $AWSCLI_DIR/dist/aws_completer

# general cleanup
rm -rf \
  /root/.cache \
  /tmp/* \
  /var/lib/apk/* \
  /var/tmp/* \
  /bootstrap.sh
