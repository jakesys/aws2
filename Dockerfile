# python 3.12+ isnt' working
#FROM python:alpine AS build
FROM python:3.11-alpine AS build

COPY build.sh /
RUN /build.sh

FROM alpine:latest
LABEL maintainer="Jake Buchholz <jakesys@jakesys.net>"

COPY --from=build /usr/local/aws-cli /usr/local/aws-cli
COPY root /
RUN /bootstrap.sh

ENTRYPOINT ["/entry"]
CMD ["--version"]

VOLUME ["/root/.aws"]
